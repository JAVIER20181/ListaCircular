package listasec;

import javax.swing.JOptionPane;

public class LISTASEC { // creacion de la clase LISTASEC

    public static void main(String args[]) {
        
        int opcion = 0, el;
        CLista lista = new CLista();
        do {
            try {
                opcion = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "1. INSERTAR DATO\n2. ELIMINAR DATO\n3. CONSULTAR SI LISTA ESTA VACIA\n4. VISUALIZAR DATOS\n" + "5. SALIR"));
                        
                switch (opcion) {
                    case 1:
                        try {
                            el = Integer.parseInt(JOptionPane.showInputDialog(null, "inserte dato"));
                            lista.InsertarDato(el);
                        } catch (NumberFormatException n) {
                            JOptionPane.showMessageDialog(null,"error"+ n.getMessage());
                        }
                        break;

                    case 2:
                       
                      try {
                            el = Integer.parseInt(JOptionPane.showInputDialog(null, "inserte dato a eliminar"));
                            lista.EliminarDato(el);
                            JOptionPane.showMessageDialog(null,"el dato eliminado es :"+ el);
                        } catch (NumberFormatException n) {
                            JOptionPane.showMessageDialog(null,"error"+ n.getMessage());
                        }
                        break;
                    case 3:
                        lista.Vacia();
                        break;
                        case 4:
                        lista.VisualizarDatos();
                        break;
                        case 5:
                            JOptionPane.showMessageDialog(null,"programa finalizado" );
                            break;
                    default:
                         JOptionPane.showMessageDialog(null,"opcion incorrecta" );


                }
            } catch (Exception e) {
            }

        } while (opcion != 5);

    }
}
