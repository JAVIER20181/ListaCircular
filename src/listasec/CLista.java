/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasec;

import javax.swing.JOptionPane;

/**
 *
 * @author MARTINEZ
 */
public class CLista {    //creacion de clase CLista

    CNodo cabeza;

    public CLista() { //el método puede ser accedido desde cualquier otro método que tenga una instancia de esta clase
        cabeza = null;
    }

    public void InsertarDato(int dat) { //el método puede ser accedido desde cualquier otro método que tenga una instancia de esta clase
        CNodo NuevoNodo;
        CNodo antes, luego;
        NuevoNodo = new CNodo();
        NuevoNodo.dato = dat;
        int ban = 0; // es una variable booleana que nos indica si ha ocurrido un suceso
        if (cabeza == null) {     //lista vacia
            NuevoNodo.siguiente = NuevoNodo;  //cambio numero 1
            cabeza = NuevoNodo;
        } else {  // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
            if (dat < cabeza.dato) { // permite controlar qué procesos tienen lugar
                NuevoNodo.siguiente = cabeza;
                antes = cabeza;
                luego = cabeza;
                do { // Primero realiza las acciones luego pregunta
                    antes = luego;
                    luego = luego.siguiente;
                } while (luego != cabeza); // indica que La iteración continuará hasta que su condición sea falsa.
                antes.siguiente = NuevoNodo;
                cabeza = NuevoNodo;
            } else {
                antes = cabeza;  // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
                luego = cabeza;
                while (ban == 0) { // indica que La iteración continuará hasta que su condición sea falsa.
                    if (dat >= luego.dato) {  // permite controlar qué procesos tienen lugar
                        antes = luego;
                        luego = luego.siguiente;
                    }
                    if (luego == cabeza) {  // permite controlar qué procesos tienen lugar
                        ban = 1;
                    } else {  // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
                        if (dat < luego.dato) { // permite controlar qué procesos tienen lugar
                            ban = 1;
                        }
                    }
                }
                antes.siguiente = NuevoNodo;
                NuevoNodo.siguiente = luego;
            }
        }
    }

    public int EliminarDato(int dat) {
        int dato=dat;
        CNodo antes, luego;
        int ban = 0;
        if (Vacia()) {  // permite controlar qué procesos tienen lugar
            System.out.print("Lista vacía "); // nos permite imprimir algo por pantalla en una ventana de consola.
        } else {
            if (dat < cabeza.dato) {  // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
                System.out.print("dato no existe en la lista "); // nos permite imprimir algo por pantalla en una ventana de consola.
            } else {  // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
                if (dat == cabeza.dato) {  // permite controlar qué procesos tienen lugar
                    if (cabeza.siguiente == cabeza) { // permite controlar qué procesos tienen lugar
                        cabeza = null;
                    } else {  // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
                        antes = cabeza;
                        luego = cabeza;
                        do { // Primero realiza las acciones luego pregunta
                            antes = luego;
                            luego = luego.siguiente;
                        } while (luego != cabeza); // indica que La iteración continuará hasta que su condición sea falsa.
                        cabeza = cabeza.siguiente;
                        antes.siguiente = cabeza;
                    }
                } else {  // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición  
                    antes = cabeza;
                    luego = cabeza;
                    while (ban == 0) { // indica que La iteración continuará hasta que su condición sea falsa.
                        if (dat > luego.dato) {  // permite controlar qué procesos tienen lugar
                            antes = luego;
                            luego = luego.siguiente;
                        } else {
                            ban = 1;  // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
                        }
                        if (luego == cabeza) {  // permite controlar qué procesos tienen lugar
                            ban = 1;
                        } else {  // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
                            if (luego.dato == dat) // permite controlar qué procesos tienen lugar 
                            {
                                ban = 1;
                            }
                        }
                    }
                    if (luego == null) { // permite controlar qué procesos tienen lugar
                        System.out.print("dato no existe en la Lista "); // nos permite imprimir algo por pantalla en una ventana de consola.
                    } else {  // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
                        if (dat == luego.dato) {  // permite controlar qué procesos tienen lugar
                            antes.siguiente = luego.siguiente;
                        } else // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
                        {
                            System.out.print("dato no existe en la Lista "); // nos permite imprimir algo por pantalla en una ventana de consola.
                        }
                    }
                }
            }
        }
        return dato;
    }

    public boolean Vacia() {  //  puede almacenar unicamente dos valores: verdadero o falso
        if (cabeza == null){
            JOptionPane.showMessageDialog(null, "LISTA VACIA");
            return true;
        } else{
            JOptionPane.showMessageDialog(null, "LISTA CON ELEMENTOS");
        }
            return false; 
         }

    public void VisualizarDatos() { //metodos
        CNodo Temporal;
        Temporal = cabeza;
        if (!Vacia()) { // permite controlar qué procesos tienen lugar
            do {
                System.out.print(" " + Temporal.dato + " ");
                Temporal = Temporal.siguiente;
            } while (Temporal != cabeza); // indica que La iteración continuará hasta que su condición sea falsa.
            System.out.println(""); // nos permite imprimir algo por pantalla en una ventana de consola.
        } else // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
        {
            System.out.print("Lista vacía"); // nos permite imprimir algo por pantalla en una ventana de consola.
        }
    }

}
